import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ProductList from "./components/ProductList.js";
import AddProduct from "./components/AddProduct.js";
import EditProduct from "./components/EditProduct.js";

function App() {
  return (
    <Router>
      <div className="container">
        <div className="colums">
          <div className="column is-half is-offset-one-quarter">

            <Routes>
              <Route exact path="/" element={<ProductList />} />
            </Routes>

            <Routes>
              <Route exact path="/add" element={<AddProduct />} />
            </Routes>
            <Routes>
              <Route exact path="/edit/:id" element={<EditProduct />} />
            </Routes>


          </div>
        </div>
      </div>
    </Router>
  );
}

export default App;
