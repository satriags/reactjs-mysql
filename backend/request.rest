GET http://localhost:5000/products

######
GET http://localhost:5000/products/3


######
POST http://localhost:5000/products
Content-Type: application/json

{
    "title": "Product 7",
    "price": 9993
}

######
PATCH http://localhost:5000/products/6
Content-Type: application/json

{
    "title": "Product 5",
    "price": 4444
}


######
DELETE http://localhost:5000/products/1